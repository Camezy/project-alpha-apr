from django.shortcuts import render, redirect
from .forms import TaskForm
from django.contrib.auth.decorators import login_required
from .models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.user = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/createtasks.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/mytasks.html", context)


# @login_required
# def my_tasks(request):
#     if request.method == "POST":
#         form = TaskForm(request.POST)
#         if form.is_valid():
#             project = form.save(False)
#             project.assignee = request.user
#             project.save()
#             return redirect("show_my_tasks")
#     else:
#         form = TaskForm()
#     context = {
#         "form": form,
#     }
#     return render(request, "tasks/mytasks.html", context)
